package andrewbastin.blot.util

import org.slf4j.impl.StaticLoggerBinder

abstract class Loggable(loggerName: String) {
    protected val logger = StaticLoggerBinder.getSingleton().loggerFactory.getLogger(loggerName)
}