package andrewbastin.blot.util

import java.time.Instant
import java.util.Date

object DateUtils {
    fun now(): Date = Date.from(Instant.now())
}