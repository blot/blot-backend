package andrewbastin.blot.util

import org.bson.internal.Base64
import java.awt.image.BufferedImage
import java.io.ByteArrayInputStream
import javax.imageio.ImageIO
import javax.xml.bind.DatatypeConverter

object BufferedImageUtils {

    fun getImageFromBase64(base64String: String): BufferedImage? {

        return try {

            val imageData = base64String.split(",")[1]
            val imageBytes = Base64.decode(imageData)

            ImageIO.read(ByteArrayInputStream(imageBytes))
        } catch (e: Exception) {

            null
        }

    }

}