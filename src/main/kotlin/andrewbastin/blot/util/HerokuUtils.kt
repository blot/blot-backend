package andrewbastin.blot.util

object HerokuUtils {

    fun getRunningPort() = System.getenv("PORT").toInt()

}