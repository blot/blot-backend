package andrewbastin.blot.util

import spark.kotlin.Http

object CORSHandler {

    fun apply(http: Http) {

        http.options("/*") {
            response.status(200)
        }

        http.before("/*") {
            response.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS")
            response.header("Access-Control-Allow-Origin", "*")
            response.header("Access-Control-Allow-Headers", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin")
            response.header("Access-Control-Allow-Credentials", "true")
        }

    }

}