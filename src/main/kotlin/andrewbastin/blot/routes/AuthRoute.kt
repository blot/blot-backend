package andrewbastin.blot.routes

import andrewbastin.blot.crypto.BlotCrypto
import andrewbastin.blot.data.db.LoginTokensDB
import andrewbastin.blot.data.db.SessionsDB
import andrewbastin.blot.data.db.UserDB
import andrewbastin.blot.data.db.content.ProfilePicContentDB
import andrewbastin.blot.extensions.onSecure
import andrewbastin.blot.data.models.request.CreateUserRequestModel
import andrewbastin.blot.extensions.onSecureAuth
import andrewbastin.blot.extensions.onSecureAuthSession
import andrewbastin.blot.extensions.onSecureAuthUser
import andrewbastin.blot.util.BufferedImageUtils
import com.beust.klaxon.Klaxon
import org.apache.commons.codec.binary.StringUtils
import org.bson.internal.Base64
import spark.HaltException
import spark.kotlin.Http
import spark.kotlin.halt

object AuthRoute: Route("auth") {

    override fun initialize(http: Http) {

        http.post("/auth/createUser") {

            request.onSecure(response) {

                try {
                    val data = Klaxon().parse<CreateUserRequestModel>(request.body()) ?: throw Exception("Invalid Data class")

                    val profilePic = BufferedImageUtils.getImageFromBase64(data.profilePictureB64) ?: throw Exception("Invalid profile picture")

                    ProfilePicContentDB.setProfilePicture(data.username, profilePic)

                    val user = UserDB.createUser(data.username, data.displayName, data.password) ?: throw Exception("Error in creating user")
                } catch (e: Exception) {
                    halt(400)
                }
            }
            ""
        }

        http.get("/auth/loginWithToken") {

            request.onSecure(response) {

                try {
                    val loginTokenString = request.headers("Authorization") ?: throw Exception("No Authorization Token")

                    val loginToken = LoginTokensDB.getToken(loginTokenString) ?: throw Exception("No such login token")
                    val user = UserDB.getUser(loginToken.issuedTo) ?: throw Exception("No such user with login token issue")

                    val session = SessionsDB.generateSessionToken(user)

                    halt(200, session)
                } catch (e: HaltException) {
                    throw e
                } catch (e: Exception) {
                    when (e.message) {
                        "No Authorization Token" -> halt(400)
                        "No such login token" -> halt(401)
                        "No such user with login token issue" -> halt(401)
                        else -> halt(400)
                    }
                }

            }

        }

        http.get("/auth/loginWithAuth") {

            request.onSecure(response) {

                val authHeader = request.headers("Authorization") ?: ""

                if (authHeader.startsWith("Basic ")) {

                    try {
                        val userNamePasswordBase64 = authHeader.split("Basic ")[1]
                        val userNamePasswordCombo = StringUtils.newStringUtf8(Base64.decode(userNamePasswordBase64)).split(":")
                        val username = userNamePasswordCombo[0]
                        val password = userNamePasswordCombo[1]

                        val storedUser = UserDB.getUser(username)
                        if (storedUser == null) {
                            halt(401)
                        } else {

                            if (BlotCrypto.verifyHash(password, storedUser.passwordHash, storedUser.passwordSalt)) {

                                val token = SessionsDB.generateSessionToken(storedUser)

                                halt(200, token)
                            } else {
                                halt(401)
                            }

                        }

                    } catch (e: Exception) {
                        if (e !is HaltException) halt(400)
                        else throw e
                    }

                } else {
                    halt(400)
                }


            }
            ""
        }

        http.get("/auth/getUser") {

            request.onSecureAuthUser(response) {

                halt(200, it.username)

            }

        }

        http.get("/auth/createLoginToken") {

            request.onSecureAuthSession(response) {

                val loginToken = LoginTokensDB.createToken(it.ownerID)

                halt(200, loginToken.token)
            }

        }

        http.delete("/auth/closeSession") {

            request.onSecureAuthSession(response) {
                SessionsDB.deleteSession(it.sessionToken)

                halt(200)
            }

        }


    }

}