package andrewbastin.blot.routes

import andrewbastin.blot.data.SchemaContext
import andrewbastin.blot.data.models.request.GQLPostQueryModel
import andrewbastin.blot.data.schemas.GQLQuerySchema
import andrewbastin.blot.data.schemas.GQLMutationSchema
import andrewbastin.blot.data.schemas.GQLSchema
import com.beust.klaxon.Klaxon
import com.github.pgutkowski.kgraphql.Context
import spark.kotlin.Http
import spark.kotlin.halt

object GQLRoute: Route("gql") {

    override fun initialize(http: Http) {

        http.post("/gql") {
            try {
                val queryObject = Klaxon().parse<GQLPostQueryModel>(request.body())
                if (queryObject != null) GQLSchema.execute(
                        queryObject.query,
                        request.queryParams("variables"),
                        Context(mapOf(SchemaContext::class to SchemaContext.createContext(request)))
                ) else halt(400)
            } catch (e: Exception) {
                halt(400, e.message ?: "")
            }
        }


    }

}