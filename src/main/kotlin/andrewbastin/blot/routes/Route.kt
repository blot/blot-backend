package andrewbastin.blot.routes

import org.slf4j.impl.StaticLoggerBinder
import spark.kotlin.Http

abstract class Route(routeName: String) {

    protected val logger = StaticLoggerBinder.getSingleton().loggerFactory.getLogger("route/$routeName")

    abstract fun initialize(http: Http)

}