package andrewbastin.blot.routes

import andrewbastin.blot.data.db.content.ProfilePicContentDB
import andrewbastin.blot.data.db.content.ProfilePicSize
import spark.HaltException
import spark.kotlin.Http
import spark.kotlin.halt
import javax.imageio.ImageIO

object ContentRoute: Route("content") {

    override fun initialize(http: Http) {


        http.get("/content/prof_pic/mini/:username") {
            try {
                val image = ProfilePicContentDB.getProfilePicture(request.params(":username"), ProfilePicSize.Mini)

                if (image != null) {
                    response.raw().outputStream.use {
                        ImageIO.write(image, "png", it)
                    }
                    response.raw().contentType = "image/png"
                } else {
                    halt(400)
                }
            } catch (e: Exception) {
                if (e !is HaltException) {

                    halt(400)

                } else throw e
            }
        }

        http.get("/content/prof_pic/original/:username") {
            try {
                val image = ProfilePicContentDB.getProfilePicture(request.params(":username"), ProfilePicSize.Original)

                if (image != null) {
                    response.raw().outputStream.use {
                        ImageIO.write(image, "png", it)
                    }
                    response.raw().contentType = "image/png"
                } else {
                    halt(400)
                }
            } catch (e: Exception) {
                if (e !is HaltException) {

                    halt(400)

                } else throw e
            }
        }

        http.get("/content/prof_pic/preview/:username") {
            try {
                val image = ProfilePicContentDB.getProfilePicture(request.params(":username"), ProfilePicSize.Preview)

                if (image != null) {
                    response.raw().outputStream.use {
                        ImageIO.write(image, "png", it)
                    }
                    response.raw().contentType = "image/png"
                } else {
                    halt(400)
                }
            } catch (e: Exception) {
                if (e !is HaltException) {

                    halt(400)

                } else throw e
            }
        }


    }



}