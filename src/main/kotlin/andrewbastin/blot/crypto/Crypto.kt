package andrewbastin.blot.crypto

import javax.crypto.SecretKeyFactory
import java.security.spec.InvalidKeySpecException
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.util.*
import javax.crypto.spec.PBEKeySpec


object BlotCrypto {

    private val secureRandom = SecureRandom()
    private const val saltChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    private const val pbeIterations = 10000
    private const val hashKeyLength = 256

    fun getSalt(length: Int = 10): String {
        val returnValue = StringBuilder(length)
        for (i in 0 until length) {
            returnValue.append(saltChars[secureRandom.nextInt(saltChars.length)])
        }
        return String(returnValue)
    }

    private fun hash(password: CharArray, salt: ByteArray): ByteArray {
        val spec = PBEKeySpec(password, salt, pbeIterations, hashKeyLength)
        Arrays.fill(password, Character.MIN_VALUE)
        try {
            val skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
            return skf.generateSecret(spec).encoded
        } catch (e: NoSuchAlgorithmException) {
            throw AssertionError("Error while hashing a password: " + e.message, e)
        } catch (e: InvalidKeySpecException) {
            throw AssertionError("Error while hashing a password: " + e.message, e)
        } finally {
            spec.clearPassword()
        }
    }

    fun generateHash(password: String, salt: String): String {
        return Base64.getEncoder().encodeToString(hash(password.toCharArray(), salt.toByteArray()))
    }

    fun verifyHash(providedPassword: String, hash: String, salt: String): Boolean {
        return generateHash(providedPassword, salt) == hash
    }


}