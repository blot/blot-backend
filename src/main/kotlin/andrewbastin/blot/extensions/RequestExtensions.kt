package andrewbastin.blot.extensions

import andrewbastin.blot.data.db.SessionsDB
import andrewbastin.blot.data.db.UserDB
import andrewbastin.blot.data.models.Session
import andrewbastin.blot.data.models.User
import spark.HaltException
import spark.Request
import spark.Response
import spark.kotlin.halt

fun Request.secure(): Boolean {

    // NOTE : localhost is marked as secure

    return  this.url().startsWith("https://") ||
            this.url().startsWith("http://localhost") ||
            this.url().startsWith("http://127.0.0.1")

}

fun Request.onSecure(res: Response, redirectIfNot: Boolean = true, func: () -> Unit) {

    if (secure()) {
        func()
    } else if (redirectIfNot) {

        val url = url().split("http://")
        res.redirect("https://$url")

    }

}

fun Request.onAuth(refreshSessionToken: Boolean = true, func: (user: User, session: Session) -> Unit) {
    try {
        val authHeader = headers("Authorization") ?: throw Exception("No Authorization")

        val session = SessionsDB.getSession(authHeader, refreshSessionToken)
        if (session != null) {
            val user = UserDB.getUser(session.ownerID)

            if (user != null) {

                func(user, session)

            } else {
                throw Exception("No such user as given in session ownerID")
            }
        }

    } catch (e: Exception) {
        if (e !is HaltException) {
            when (e.message) {
                "No Authorization" -> halt(401)
                "No Such Session" -> halt(401)
                "No such user as given in session ownerID" -> halt(500)
                else -> halt(400)
            }
        } else {
            throw e
        }
    }
}

fun Request.onAuthSession(refreshSessionToken: Boolean = true, func: (session: Session) -> Unit) {
    try {
        val authHeader = headers("Authorization") ?: throw Exception("No Authorization")

        val session = SessionsDB.getSession(authHeader, refreshSessionToken)
        if (session != null) {
            func(session)
        }

    } catch (e: Exception) {
        if (e !is HaltException) {
            when (e.message) {
                "No Authorization" -> halt(401)
                "No Such Session" -> halt(401)
                else -> halt(400)
            }
        } else {
            throw e
        }
    }
}

fun Request.onAuthUser(refreshSessionToken: Boolean = true, func: (user: User) -> Unit) {
    try {
        val authHeader = headers("Authorization") ?: throw Exception("No Authorization")

        val session = SessionsDB.getSession(authHeader, refreshSessionToken)
        if (session != null) {
            val user = UserDB.getUser(session.ownerID)

            if (user != null) {

                func(user)

            } else {
                throw Exception("No such user as given in session ownerID")
            }
        }

    } catch (e: Exception) {
        if (e !is HaltException) {
            when (e.message) {
                "No Authorization" -> halt(401)
                "No Such Session" -> halt(401)
                "No such user as given in session ownerID" -> halt(500)
                else -> halt(400)
            }
        } else {
            throw e
        }
    }
}

fun Request.onSecureAuth(res: Response, refreshSessionToken: Boolean = true, func: (user: User, session: Session) -> Unit) {

    onSecure(res) {
        onAuth(refreshSessionToken, func)
    }

}

fun Request.onSecureAuthSession(res: Response, refreshSessionToken: Boolean = true, func: (session: Session) -> Unit) {

    onSecure(res) {
        onAuthSession(refreshSessionToken, func)
    }

}

fun Request.onSecureAuthUser(res: Response, refreshSessionToken: Boolean = true, func: (user: User) -> Unit) {

    onSecure(res) {
        onAuthUser(refreshSessionToken, func)
    }

}