package andrewbastin.blot.data.models

import org.bson.codecs.pojo.annotations.BsonId

data class User(
        @BsonId
        val username: String,
        val displayName: String,
        val passwordHash: String,
        val passwordSalt: String
) {

    override fun equals(other: Any?): Boolean {
        return if (other is User) username == other.username else false
    }

}