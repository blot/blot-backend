package andrewbastin.blot.data.models.request

data class CreateUserRequestModel(
        val username: String,
        val displayName: String,
        val password: String,
        val profilePictureB64: String
)