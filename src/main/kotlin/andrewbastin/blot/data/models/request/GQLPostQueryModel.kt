package andrewbastin.blot.data.models.request

data class GQLPostQueryModel(
        val query: String
)