package andrewbastin.blot.data.models

import org.bson.codecs.pojo.annotations.BsonId
import java.util.*

data class Session(
        @BsonId
        val sessionToken: String,
        val ownerID: String,
        val issuedOn: Date,
        val lastRefreshedOn: Date = issuedOn
)