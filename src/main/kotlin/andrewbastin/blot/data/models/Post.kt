package andrewbastin.blot.data.models

import org.bson.codecs.pojo.annotations.BsonId
import java.util.*


data class Post(
        @BsonId
        val id: String,
        val postedOn: Date,
        val postedBy: String,
        val contentB64: String
)