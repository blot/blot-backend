package andrewbastin.blot.data.models

import org.bson.codecs.pojo.annotations.BsonId
import java.util.*

data class LoginToken(
        @BsonId
        val token: String,
        val issuedTo: String,
        val issuedAt: Date,
        val lastRefreshedAt: Date = issuedAt
)