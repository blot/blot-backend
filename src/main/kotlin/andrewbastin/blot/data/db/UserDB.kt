package andrewbastin.blot.data.db

import andrewbastin.blot.crypto.BlotCrypto
import andrewbastin.blot.data.models.User
import org.litote.kmongo.findOneById

object UserDB: DataDB<User>("users", User::class.java) {

    fun getUser(id: String): User? {
        return collection.findOneById(id)
    }

    fun createUser(id: String, displayName: String, password: String): User? {
        if (getUser(id) == null) {

            val passwordSalt = BlotCrypto.getSalt()
            val passwordHash = BlotCrypto.generateHash(password, passwordSalt)

            val user = User(id, displayName, passwordHash, passwordSalt)

            collection.insertOne(user)

            return user
        } else {
            logger.warn("Attempt to create a user with existing username $id")
            return null
        }
    }

}