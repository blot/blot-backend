package andrewbastin.blot.data.db

import andrewbastin.blot.crypto.BlotCrypto
import andrewbastin.blot.data.models.Session
import andrewbastin.blot.data.models.User
import andrewbastin.blot.util.DateUtils
import org.litote.kmongo.deleteOneById
import org.litote.kmongo.dropIndex
import org.litote.kmongo.findOneById
import org.litote.kmongo.updateOneById

object SessionsDB: DataDB<Session>("sessions", Session::class.java) {

    fun deleteSession(token: String) {
        collection.deleteOneById(token)
    }

    fun generateSessionToken(user: User): String {
        val token = generateUniqueToken()

        val session = Session(token, user.username, DateUtils.now())
        collection.insertOne(session)
        
        return token
    }

    fun getSession(token: String, refreshSessionToken: Boolean = true): Session? {
        val session = collection.findOneById(token)

        return if (session != null) {

            if (refreshSessionToken) {
                val updatedSession = generateRefreshedSession(session)
                collection.updateOneById(session.sessionToken, updatedSession)

                updatedSession
            } else session

        } else null
    }

    private fun generateUniqueToken(): String {
        var token = BlotCrypto.getSalt(25)
        while (getSession(token, false) != null) {
            logger.warn("Unique Token registration collision with id $token")
            token = BlotCrypto.getSalt(25)
        }

        return token
    }

    private fun generateRefreshedSession(session: Session): Session {
        return session.copy(lastRefreshedOn = DateUtils.now())
    }

}