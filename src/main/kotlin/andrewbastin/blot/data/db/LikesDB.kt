package andrewbastin.blot.data.db

import org.bson.Document
import org.litote.kmongo.MongoOperator.*
import org.litote.kmongo.aggregate
import org.litote.kmongo.count
import org.litote.kmongo.updateOne

object LikesDB: DataDB<Document>("likes", Document::class.java) {

    fun getLikeCount(postID: String): Int {
        try {
            val doc = collection.aggregate<Document>(
                    """{ $match: { "_id": "$postID" } }""",
                    """{ $limit: 1 }""",
                    "{ $project: { count: { $size: '\$likes' } } }"
            ).firstOrNull()

            return if (doc != null) doc.getInteger("count") else 0
        } catch (e: Exception) {
            logger.warn("Exception occured while getting like count for post with ID '$postID' Message : ${e.message}")
            return 0
        }
    }

    fun likePost(username: String, postID: String) {
        if (isPostLikedByUser(username, postID)) throw Exception("Post was already liked by user")
        else {

            if (collection.count("""{ "_id": "$postID" }""") == 0L) { // Post didn't exist yet, so this is the first like
                val docMap = mapOf("_id" to postID, "likes" to listOf(username))
                collection.insertOne(Document(docMap))
            } else {
                collection.updateOne("""{ "_id": "$postID" }""", """{ $push: { "likes": "$username" }}""")
            }

        }
    }

    fun unlikePost(username: String, postID: String) {
        if (collection.count("""{ "_id": "$postID", "likes": "$username" }""") == 0L) throw Exception("Post is not liked by the user")
        else {
            collection.updateOne("""{ "_id": "$postID" }""", """{ $pull: { "likes": "$username" }}""")

        }
    }

    fun isPostLikedByUser(username: String, postID: String): Boolean {
        return collection.count("""{ "_id": "$postID", "likes": "$username" }""") > 0
    }

}