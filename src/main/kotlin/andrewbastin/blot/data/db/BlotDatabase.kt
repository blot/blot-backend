package andrewbastin.blot.data.db

import andrewbastin.blot.IN_PRODUCTION
import com.mongodb.MongoClient
import com.mongodb.MongoClientOptions
import com.mongodb.MongoCredential
import com.mongodb.ServerAddress
import org.litote.kmongo.KMongo

val PROD_DB_ADDRESS = ServerAddress("ds139459.mlab.com", 39459)
val PROD_DB_CRED = MongoCredential.createCredential("blot-openshift-1", "blot", "9CjY0FVA7ll5jIryTCv5".toCharArray())

val PROD_CONTENT_ADDRESS = ServerAddress("ds139459.mlab.com", 39219)
val PROD_CONTENT_CRED = MongoCredential.createCredential("blot-openshift-1", "blot-content", "9CjY0FVA7ll5jIryTCv5".toCharArray())

val GQLDBClient = if (IN_PRODUCTION) KMongo.createClient(PROD_DB_ADDRESS, listOf(PROD_DB_CRED)) else KMongo.createClient()
val GQLBlotDB = GQLDBClient.getDatabase("blot") ?: throw Exception("GQLBlotDB is null. Make sure the DB is turned on...")

val GQLDBClientAsync = if (IN_PRODUCTION) org.litote.kmongo.async.KMongo.createClient(
        "mongodb://blot-openshift-1:9CjY0FVA7ll5jIryTCv5@ds139459.mlab.com:39459/blot"
) else org.litote.kmongo.async.KMongo.createClient()
val GQLBlotDBAsync = GQLDBClientAsync.getDatabase("blot") ?: throw Exception("GQLBlotDBAsync is null. Make sure the DB is turned on...")

val GridDBClient = if (IN_PRODUCTION) MongoClient(PROD_CONTENT_ADDRESS, PROD_CONTENT_CRED, MongoClientOptions.builder().build()) else MongoClient()
val GridBlotDB = GridDBClient.getDatabase("blot-content")