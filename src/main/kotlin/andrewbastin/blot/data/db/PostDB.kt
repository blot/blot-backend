package andrewbastin.blot.data.db

import andrewbastin.blot.data.models.Post
import andrewbastin.blot.data.models.User
import andrewbastin.blot.util.DateUtils
import org.apache.commons.lang3.RandomStringUtils
import org.litote.kmongo.find
import org.litote.kmongo.findOneById
import org.litote.kmongo.sort
import java.util.*

object PostDB: DataDB<Post>("posts", Post::class.java) {

    fun getPost(id: String): Post? {
        return collection.findOneById(id)
    }

    fun createPost(contentB64: String, user: User, postDate: Date = DateUtils.now()): Post {
        val post = Post(
                generateUniquePostID(),
                postDate,
                user.username,
                contentB64
        )
        collection.insertOne(post)

        return post
    }

    private fun generateUniquePostID(): String {
        var id = RandomStringUtils.random(8, true, true)
        while (getPost(id) != null) {
            logger.warn("Unique ID Generation Collision occurred with $id")
            id = RandomStringUtils.random(8, true, true)
        }

        return id
    }

    fun getPostsByUser(id: String, offset: Int, limit: Int): List<Post> {
        val list = collection.find("""{ "postedBy": "$id" }""").sort("""{ "postedOn": -1 }""").skip(offset).limit(limit).toList()

        return list
    }

}