package andrewbastin.blot.data.db

import andrewbastin.blot.util.Loggable
import com.mongodb.BasicDBList
import org.bson.Document
import org.litote.kmongo.*
import org.litote.kmongo.MongoOperator.*

object FollowersDB: DataDB<Document>("followers", Document::class.java) {

    fun getFollowerCount(username: String): Int {
        try {
            val doc = collection.aggregate<Document>(
                    """{ $match: { "_id": "$username" } }""",
                    """{ $limit: 1 }""",
                    "{ $project: { count: { $size: '\$followers' } } }"
            ).firstOrNull()

            return if (doc != null) doc.getInteger("count") else 0
        } catch (e: Exception) {
            logger.warn("Exception occured while getting follower count for '$username' Message : ${e.message}")
            return 0
        }
    }

    fun getFollowedBy(username: String, toCheckUsername: String): Boolean {
        val followDoc = collection.findOne("""{ "_id": "$username", "followers": "$toCheckUsername" }""")

        return followDoc != null
    }

    fun addFollower(username: String, toAddUsername: String) {
        if (collection.findOneById(username) == null) {
            collection.insertOne(Document.parse("""{ "_id": "$username", "followers": ["$toAddUsername"] }"""))
        } else {
            collection.updateOneById(username, """{$push: {"followers": "$toAddUsername"}}""")
        }
    }

    fun removeFollower(username: String, toRemoveUsername: String) {
        if (collection.findOneById(username) == null) throw Exception("User doesn't follow anybody")
        else {
            collection.updateOne("""{ "_id": "$username" }""", """{ $pull: { "followers": "$toRemoveUsername"} }""")
        }
    }

    fun getFollowersIterable(username: String): ArrayList<String>? {
        val data = collection.findOneById(username)?.get("followers") as ArrayList<String>?
        return data
    }

}