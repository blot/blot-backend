package andrewbastin.blot.data.db

import andrewbastin.blot.data.models.Post
import com.mongodb.BasicDBList
import org.bson.Document
import org.litote.kmongo.*
import org.litote.kmongo.MongoOperator.slice
import org.litote.kmongo.MongoOperator.push
import org.litote.kmongo.MongoOperator.each
import org.litote.kmongo.MongoOperator.position
import org.litote.kmongo.async.updateOneById

object FeedsDB: DataDB<Document>("feeds", Document::class.java) {

    fun getFeedPosts(username: String, offset: Int, limit: Int): List<Post>? {
        try {
            val feed = collection.find(
                    """{"_id": "$username"}, { "_id": 0, "feed": {$slice: [$offset, $limit]}}"""
            ).first() ?: return emptyList()

            val list = feed["feed"] as List<String>?
            val postList = mutableListOf<Post>()
            if (list != null) {

                list.forEach {
                    val post = PostDB.getPost(it)
                    if (post != null) postList.add(post)
                }

                return postList

            } else return null
        } catch (e: Exception) {
            logger.error(e.toString())
            return null
        }
    }

    fun publishToFeed(username: String, post: Post) {
        if (collection.count("""{ "_id": "$username" }""") > 0) {
            collection.updateOneById(username, """{$push: { "feed": { $each: ["${post.id}"], $position: 0 }}""")
        } else {
            val docMap = mapOf("_id" to username, "feed" to arrayOf(post.id))
            val doc = Document(docMap)
            collection.insertOne(doc)
        }
    }

    fun publishToFeedAsync(username: String, post: Post) {
        if (collection.count("""{ "_id": "$username" }""") > 0) {
            collectionAsync.updateOneById(username, """{$push: { "feed": { $each: ["${post.id}"], $position: 0 }}}""") { upRes, throwable ->
                // Nothing here :P
                logger.info("${upRes} ${throwable}")
            }
        } else {
            val docMap = mapOf("_id" to username, "feed" to listOf(post.id))
            val doc = Document(docMap)
            collectionAsync.insertOne(doc) { _, _ ->
                // Nothing here :P
            }
        }
    }

    fun publishToFollowers(followersOfUsername: String, post: Post) {
        try {
            val followersIterable = FollowersDB.getFollowersIterable(followersOfUsername)

            if (followersIterable != null) {
                for (follower in followersIterable) {
                    publishToFeedAsync(follower, post)
                }
            }
        } catch (e: Exception) {
            logger.error("Publishing post with ID '${post.id}' to feeds failed with ${e::class.java.name} having message : ${e.message}")
        }
    }

}