package andrewbastin.blot.data.db

import andrewbastin.blot.util.Loggable

abstract class DataDB<T>(private val collectionName: String, collectionClass: Class<T>): Loggable("DB/$collectionName") {

    protected val collection = GQLBlotDB.getCollection<T>(collectionName, collectionClass)
    protected val collectionAsync = GQLBlotDBAsync.getCollection<T>(collectionName, collectionClass)
}