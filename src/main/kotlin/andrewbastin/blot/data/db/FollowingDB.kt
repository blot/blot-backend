package andrewbastin.blot.data.db

import org.bson.Document
import org.litote.kmongo.*
import org.litote.kmongo.MongoOperator.*

object FollowingDB: DataDB<Document>("following", Document::class.java) {

    fun getFollowingCount(username: String): Int {
        try {
            val doc = collection.aggregate<Document>(
                    """{ $match: { "_id": "$username" } }""",
                    "{ $limit: 1 }",
                    "{ $project: { count: { $size: '\$following' } } }"
            ).firstOrNull()

            return if (doc != null) doc.getInteger("count") else 0
        } catch (e: Exception) {
            logger.warn("Exception occured while getting follower count for '$username' Message : ${e.message}")
            return 0
        }

    }

    fun getIfFollowing(username: String, toCheckUsername: String): Boolean {
        val followDoc = collection.findOne("""{ "_id": "$username", "following": "$toCheckUsername" }""")

        return followDoc != null
    }

    fun addFollowing(username: String, toAddUsername: String) {
        if (collection.findOneById(username) == null) {
            collection.insertOne(Document.parse("""{ "_id": "$username", "following": ["$toAddUsername"] }"""))
        } else {
            collection.updateOneById(username, """{$push: {"following": "$toAddUsername"}}""")
        }
    }

    fun removeFollowing(username: String, toRemoveUsername: String) {
        if (collection.findOneById(username) == null) throw Exception("User doesn't follow anybody")
        else {
            collection.updateOne("""{ "_id": "$username" }""", """{ $pull: { "following": "$toRemoveUsername" } }""")
        }
    }

}