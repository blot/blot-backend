package andrewbastin.blot.data.db.content

import andrewbastin.blot.data.db.GridBlotDB
import andrewbastin.blot.util.Loggable
import com.mongodb.client.gridfs.GridFSBuckets


abstract class ContentDB(private val bucketName: String): Loggable("content/$bucketName") {

    protected val bucket = GridFSBuckets.create(GridBlotDB, bucketName)

}