package andrewbastin.blot.data.db.content

import andrewbastin.blot.extensions.getInputStream
import net.coobird.thumbnailator.Thumbnails
import java.awt.image.BufferedImage
import javax.imageio.ImageIO

object ProfilePicContentDB: ContentDB("prof_pic") {

    fun getProfilePicture(username: String, size: ProfilePicSize): BufferedImage? {

        try {
            return ImageIO.read(bucket.openDownloadStreamByName("${size.name.toLowerCase()}_$username"))
        } catch (e: Exception) {
            logger.warn("Exception occured when trying to fetch profile image with username '$username' with message '${e.message}'")

            return null
        }

    }


    fun setProfilePicture(username: String, sourceImage: BufferedImage) {
        val miniImage = Thumbnails.of(sourceImage).size(64, 64).asBufferedImage()
        val previewImage = Thumbnails.of(sourceImage).size(128, 128).asBufferedImage()

        bucket.uploadFromStream("mini_$username", miniImage.getInputStream())
        bucket.uploadFromStream("preview_$username", previewImage.getInputStream())
        bucket.uploadFromStream("original_$username", sourceImage.getInputStream())
    }

}

enum class ProfilePicSize {

    Original, // Full Profile Picture (source dimensions)
    Mini, // Smallest version (64 x 64). To be used in lists and stuff
    Preview // Bigger size (128 x 128). To be used in user pages and stuff

}