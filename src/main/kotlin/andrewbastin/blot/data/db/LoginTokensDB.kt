package andrewbastin.blot.data.db

import andrewbastin.blot.crypto.BlotCrypto
import andrewbastin.blot.data.models.LoginToken
import andrewbastin.blot.util.DateUtils
import org.litote.kmongo.findOneById
import org.litote.kmongo.updateOneById

object LoginTokensDB: DataDB<LoginToken>("logintokens", LoginToken::class.java) {

    fun getToken(id: String, refreshToken: Boolean = true): LoginToken? {
        val token = collection.findOneById(id)
        return if (refreshToken && token != null) LoginTokensDB.refreshToken(token) else token
    }

    private fun refreshToken(token: LoginToken): LoginToken {
        val newToken = token.copy(lastRefreshedAt = DateUtils.now())
        collection.updateOneById(token.token, newToken)
        return newToken
    }

    fun createToken(user: String): LoginToken {
        val tokenID = createUniqueToken()
        val token = LoginToken(tokenID, user, DateUtils.now())
        collection.insertOne(token)

        return token
    }

    private fun createUniqueToken(): String {
        var token = BlotCrypto.getSalt(25)
        while (getToken(token, false) != null) {
            logger.warn("Unique token generation collision occurred $token")
            token = BlotCrypto.getSalt(25)
        }
        return token
    }

}