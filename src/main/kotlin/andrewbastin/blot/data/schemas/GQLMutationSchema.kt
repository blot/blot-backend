package andrewbastin.blot.data.schemas

import andrewbastin.blot.data.SchemaContext
import andrewbastin.blot.data.db.*
import andrewbastin.blot.data.models.Post
import andrewbastin.blot.data.models.User
import com.github.pgutkowski.kgraphql.Context
import com.github.pgutkowski.kgraphql.KGraphQL
import com.github.pgutkowski.kgraphql.schema.dsl.SchemaBuilder
import org.apache.commons.codec.binary.StringUtils
import org.bson.internal.Base64
import spark.kotlin.halt
import java.util.*

object GQLMutationSchema {

    fun <T : Any> declareMutations(schemaBuilder: SchemaBuilder<T>) {

        schemaBuilder.apply {

            mutation("createPost") {
                resolver { ctx: Context, contentB64: String ->

                    val context = ctx[SchemaContext::class]

                    if (context != null) {

                        val user = context.user
                        if (user != null) {

                            val post = PostDB.createPost(contentB64, user)
                            FeedsDB.publishToFollowers(post.postedBy, post)

                            post
                        } else {
                            throw halt(401)
                        }

                    } else {
                        throw Exception("No Context In Request")
                    }

                }
            }

            mutation("followUser") {

                resolver { ctx: Context, id: String ->
                    val context = ctx[SchemaContext::class] ?: throw Exception("No Context In Request")

                    val user = context.user ?: throw halt(401)

                    if (UserDB.getUser(id) != null) {
                        if (id == user.username && FollowingDB.getIfFollowing(user.username, id)) throw halt(400)
                        else {
                            FollowingDB.addFollowing(user.username, id)
                            FollowersDB.addFollower(id, user.username)

                            UserDB.getUser(id)
                        }
                    } else throw halt(401)
                }

            }

            mutation("unfollowUser") {
                resolver { ctx: Context, id: String ->
                    val context = ctx[SchemaContext::class] ?: throw Exception("No Context In Request")

                    val user = context.user ?: throw halt(401)

                    if (UserDB.getUser(id) != null) {
                        if (id == user.username || !FollowingDB.getIfFollowing(user.username, id)) throw halt(400)
                        else {
                            FollowingDB.removeFollowing(user.username, id)
                            FollowersDB.removeFollower(id, user.username)

                            UserDB.getUser(id)
                        }
                    } else throw halt(400)
                }
            }

            mutation("likePost") {
                resolver { ctx: Context, id: String ->
                    val context = ctx[SchemaContext::class] ?: throw Exception("No Context In Request")
                    val user = context.user ?: throw halt(401)

                    LikesDB.likePost(user.username, id)

                    PostDB.getPost(id)
                }
            }

            mutation("unlikePost") {
                resolver { ctx: Context, id: String ->
                    val context = ctx[SchemaContext::class] ?: throw Exception("No Context In Request")
                    val user = context.user ?: throw halt(401)

                    LikesDB.unlikePost(user.username, id)

                    PostDB.getPost(id)
                }
            }


        }

    }

}