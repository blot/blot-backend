package andrewbastin.blot.data.schemas

import andrewbastin.blot.data.SchemaContext
import andrewbastin.blot.data.db.*
import andrewbastin.blot.data.models.Post
import andrewbastin.blot.data.models.User
import com.github.pgutkowski.kgraphql.Context
import com.github.pgutkowski.kgraphql.KGraphQL
import com.github.pgutkowski.kgraphql.schema.dsl.SchemaBuilder
import spark.kotlin.halt

object GQLQuerySchema {

    fun <T: Any> declareQueries(schemaBuilder: SchemaBuilder<T>) {

        schemaBuilder.apply {

            query("post") {
                resolver { id: String ->
                    PostDB.getPost(id) ?: throw Exception("No post exists with the id '$id'")
                }
            }

            query("user") {
                resolver { id: String ->
                    UserDB.getUser(id) ?: throw Exception("No user exists with the id '$id'")
                }
            }

            query("me") {
                resolver { ctx: Context ->
                    val context = ctx[SchemaContext::class]

                    context?.user
                }
            }

            query("getBlotsByUser") {
                resolver { id: String, offset: Int, limit: Int ->
                    if (limit > 10 || limit < 0 || offset < 0) {
                        listOf<Post>()
                    } else {
                        return@resolver PostDB.getPostsByUser(id, offset, limit)
                    }
                }
            }

            query("getUserFeed") {
                resolver { offset: Int, limit: Int, ctx: Context ->
                    val context = ctx[SchemaContext::class]

                    if (context?.user != null) {

                        FeedsDB.getFeedPosts(context.user.username, offset, limit)

                    } else {
                        throw halt(400)
                    }
                }
            }

        }

    }

}