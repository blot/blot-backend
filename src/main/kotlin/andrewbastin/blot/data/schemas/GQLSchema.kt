package andrewbastin.blot.data.schemas

import com.github.pgutkowski.kgraphql.KGraphQL

val GQLSchema = KGraphQL.schema {

    configure {
        useDefaultPrettyPrinter = true
    }

    GQLQuerySchema.declareQueries(this)
    GQLMutationSchema.declareMutations(this)

    GQLTypeSchema.declareTypes(this)
}