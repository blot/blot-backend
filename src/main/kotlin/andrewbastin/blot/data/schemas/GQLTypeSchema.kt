package andrewbastin.blot.data.schemas

import andrewbastin.blot.data.SchemaContext
import andrewbastin.blot.data.db.*
import andrewbastin.blot.data.models.Post
import andrewbastin.blot.data.models.User
import com.github.pgutkowski.kgraphql.Context
import com.github.pgutkowski.kgraphql.schema.dsl.SchemaBuilder
import javax.xml.validation.Schema

object GQLTypeSchema {

    fun <T: Any> declareTypes(schemaBuilder: SchemaBuilder<T>) {

        schemaBuilder.apply {

            type<Post> {

                Post::postedOn.ignore() // Posted On is date.... Transforming it into a epoch string

                property<String>("postedOn") {

                    resolver {
                        it.postedOn.time.toString()
                    }

                }


                Post::postedBy.ignore() // Replacing it with user objects

                property<User?>("postedBy") {

                    resolver {
                        UserDB.getUser(it.postedBy)
                    }

                }

                property<Int>("likeCount") {

                    resolver {
                        LikesDB.getLikeCount(it.id)
                    }

                }

                property<Boolean>("likedByMe") {

                    resolver { post: Post, ctx: Context ->
                        val context = ctx[SchemaContext::class] ?: throw Exception("No Context in Request")

                        if (context.user == null) false
                        else LikesDB.isPostLikedByUser(context.user.username, post.id)
                    }

                }
            }

            type<User> {
                User::passwordHash.ignore()
                User::passwordSalt.ignore()

                property<Int>("followerCount") {
                    resolver {
                        FollowersDB.getFollowerCount(it.username)
                    }
                }

                property<Int>("followingCount") {
                    resolver {
                        FollowingDB.getFollowingCount(it.username)
                    }
                }

                property<Boolean>("followedByMe") {
                    resolver { user: User, ctx: Context ->
                        val context = ctx[SchemaContext::class]

                        if (context != null) {

                            when {
                                context.user == user -> true
                                context.user != null -> FollowingDB.getIfFollowing(context.user.username, user.username)
                                else -> false
                            }

                        } else false
                    }
                }

                property<Boolean>("followsMe") {
                    resolver { user: User, ctx: Context ->
                        val context = ctx[SchemaContext::class]

                        if (context != null) {

                            when {
                                context.user == user -> true
                                context.user != null -> FollowersDB.getFollowedBy(context.user.username, user.username)
                                else -> false
                            }

                        } else false
                    }
                }

            }

        }

    }

}