package andrewbastin.blot.data

import andrewbastin.blot.data.db.SessionsDB
import andrewbastin.blot.data.db.UserDB
import andrewbastin.blot.data.models.Session
import andrewbastin.blot.data.models.User
import spark.Request

data class SchemaContext(
        val request: Request,
        val userSession: Session?,
        val user: User?
) {

    companion object {

        fun createContext(req: Request): SchemaContext {

            val sessionToken = req.headers("Authorization")
            val session = if (sessionToken != null) SessionsDB.getSession(sessionToken) else null
            val user = if (session != null) UserDB.getUser(session.ownerID) else null


            return SchemaContext(
                    req,
                    session,
                    user
            )
        }

    }

}