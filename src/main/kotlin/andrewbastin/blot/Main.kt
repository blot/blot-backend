package andrewbastin.blot

import andrewbastin.blot.routes.AuthRoute
import andrewbastin.blot.routes.ContentRoute
import andrewbastin.blot.routes.GQLRoute
import andrewbastin.blot.util.CORSHandler
import andrewbastin.blot.util.HerokuUtils
import spark.kotlin.ignite

val IN_PRODUCTION = true

fun main(args: Array<String>) {

    if (IN_PRODUCTION) for (i in 1..1000) println("Server on Production Mode!!!! Beware")

    val http = ignite()

    http.port(if (IN_PRODUCTION) HerokuUtils.getRunningPort() else 2299)

    // region Routes
    CORSHandler.apply(http)

    GQLRoute.initialize(http)
    AuthRoute.initialize(http)
    ContentRoute.initialize(http)
    // endregion
}